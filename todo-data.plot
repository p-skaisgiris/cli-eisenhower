# Set output to png
#set terminal pngcairo

# Set output to ASCII
set terminal dumb size 90, 25

# Read CSV
set datafile separator comma

# Set title of the graph
set title 'Eisenhower matrix for TODOs'

# Set label of x-axis
set xlabel 'Urgency'

# Set label of y-axis
set ylabel 'Importance'
set ylabel offset -3,0

# x axis ranges from 0 to 10
set xrange [0:10]
# y axis ranges from 0 to 10
set yrange [0:10]

# Draw x intercept at x = 5
set arrow from 5,0 to 5,10 nohead
# Draw y intercept at y = 5
set arrow from 0,5 to 10,5 nohead

set lmargin 15

set offset 1,1,1,1
plot 'todo-data.csv' using 1:2:3 with labels point pt 7 offset char 1,1 notitle
