while test $# -gt 0; do
  case "$1" in
    -h|--help)
      echo "cli-eisenhower - display pending tasks rated by urgency and importance (Eisenhower matrix) and output a prioritized todo file (useful for calcurse)."
      echo " "
	  echo "Eisenhower matrix is a system that rates pending tasks by urgency and importance, both in the scale of 0 to 10. It looks as follows:"
	  echo "             __________________________ "
	  echo "            |            |             |"
	  echo "     ^      |  Schedule  |  Do first   |"
	  echo "     |      |____________|_____________|"
	  echo " Importance |            |             |"
	  echo "            |  Don't do  |  Delegate   |"
	  echo "            |____________|_____________|"
	  echo "                     Urgency -->        "
	  echo " "
	  echo "Do first: First focus on important tasks to be done the same day (urgent & important)"
	  echo "Schedule: Important, but not-so-urgent stuff that should be scheduled"
	  echo "Delegate: What's urgent, but less important stuff, try to delegate to others"
	  echo "Don't do: What's neither urgent, nor important, don't do at all"
	  echo " "
      echo "cli-eisenhower [options]"
      echo " "
      echo "Options:"
      echo "-h, --help                show brief help"
      echo "-t, --task                description of a task, e.g. Run. Iit's better to have short descriptions as it will look better in the visualization."
	  echo "-u, --urgency             the amount of effort needed for the task. Graded 0-10"
	  echo "-i, --importance          the amount of impact a task will provide, how useful is the task, in your opinion. Graded 0-10"
	  echo "-c, --calurse-dir=DIR     specify a directory for calcurse and where a prioritized todo file would be dumped"
      exit 0
      ;;
    -t|--task)
      shift
      if test $# -gt 0; then
        export TASK=$1
      else
        echo "No task specified."
        exit 1
      fi
      shift
      ;;
	-u|--urgency)
	  shift
	  if test $# -gt 0; then
		export URGENCY=$1
	  else
		echo "No effort for the task specified."
		exit 1
	  fi
	  shift
	  ;;
	-i|--importance)
	  shift
	  if test $# -gt 0; then
		export IMPORTANCE=$1
	  else 
		echo "No impact of task specified."
		exit 1
	  fi
	  shift
	  ;;
	-c)
	  shift
	  if test $# -gt 0; then
	    export CALDIR=$1
	  else
	    echo "No calcurse directory specified."
	    exit 1
	  fi
	  shift
	  ;;
	--calcurse-dir*)
	  export CALDIR=`echo $1 | sed -e 's/^[^=]*=//g'`
	  shift
	  ;;
    *)
	  echo "Flag $1 unrecognized."
	  exit 1
      break
      ;;
  esac
done

if [[ -z "$TASK" ]] | [[ -z "$URGENCY" ]] | [[ -z "$IMPORTANCE" ]]; then
	echo "At least one of the flags is empty. Not appending new information."
else 
	# Write the new task to a file to be plotted
	echo "$URGENCY,$IMPORTANCE,$TASK" >> todo-data.csv
fi


# Plot the new Eisenhower matrix
gnuplot todo-data.plot

# TODO: Logic to prioritize TODOs

if ! [[ -z $CALDIR ]]; then
	# Output a prioritized todo file for calcurse here
	echo "[$URGENCY] $TASK" >> $CALDIR
fi
