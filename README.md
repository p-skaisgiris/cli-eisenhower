# cli-eisenhower

This repository holds miniscule bash and gnuplot scripts that are supposed to help you prioritize TODOs and increase your productivity by visualizing your tasks in an [Eisenhower matrix](https://www.eisenhower.me/eisenhower-matrix/). The scripts have the following functionality: insert tasks, rate them by urgency and importance and plot using gnuplot.

### Requirements

Make sure you have `bash` and `gnuplot` installed.

### Usage

First make the script an executable by running `sudo chmod +x eisenhower.sh`.

Then running
```
./eisenhower.sh -t Jog -u 4 -i 7
```
will create a file `todo-data.csv` (if it has not been created already) and insert the task Jog with urgency 4 and importance 7 into the file.

Running 
```
./eisenhower.sh
```
when `todo-data.csv` has already been created and thus has some tasks inside and their corresponding urgency and importance ratings, will only plot the data, not append anything to a file. Same behaviour could be achieved by running `gnuplot todo-data.plot`

Run `./eisenhower.sh -h` for more information.

### Preview

![Example](example.jpg)

### TODO

Output a prioritized todo file that could be used by calcurse in its TODO section.
